```
USAGE
       bluetooth-autoconnect [option]

OPTIONS
          --daemon
              Connects as soon as an adapter powers on.

          --verbose
              Shows detailed log messages.
